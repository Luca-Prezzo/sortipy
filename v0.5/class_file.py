import os
import shutil



def list_file(path, hidden=False):
    if hidden:
        return [i for i in os.listdir(path) if os.path.isfile(path + "/" + i) and i[0] != "."]
    else:
        return [i for i in os.listdir(path) if os.path.isfile(path + "/" + i)]


def mkcopy(path):
    try:
        shutil.copytree(path, path+"_BACKUP")
    except FileExistsError:
        print("+- [!] -+ Backup folder already exists.")


class File:
    
    
    def __init__(self, path, filename):
        self.path = path
        self.filename = filename
        self.name = self.__get_name()
        self.extension = self.__get_extension()
        self.filetype = self.__get_filetype()        
        
    
    # return the name for the file
    def __get_name(self):
        
        # split the filename where is the "." and then return the left part
        return os.path.splitext(self.filename)[0]
    
    
    # return the extension for the file
    def __get_extension(self):
        
        # split the filename where is the "." and then return the right part
        return os.path.splitext(self.filename)[1].replace(".", "").lower()
    
    
    # return the type for the file
    def __get_filetype(self):
        
        # all the filetype and relative extension
        filetype = {
            "testi": [
                "txt",
                "pdf",
                "docx",
                "doc",
                "tex",
                "md",
                "odt",
                "json",
                "wg"
            ],
            "adobe": [
                "ai",
                "psd",
                "propj",
                "svg",
                "eps"
            ],
            "foto": [
                "png",
                "jpeg",
                "jpg",
                "bpm",
                "png",
                "svg",
                "tif",
                "tiff"
            ],
            "video": [
                "mp4",
                "3gp",
                "mkv",
                "avi",
                "gif",
                "mpeg",
                "mpg",
                "webm"
            ],
            "audio": [
                "aif",
                "cda",
                "mid",
                "mp3",
                "mpa",
                "ogg",
                "wav",
                "wma",
                "wpl"
            ],
            "archivio": [
                "7z",
                "deb",
                "pkg",
                "rpm",
                "gz",
                "z",
                "zip",
                "tar"
            ],
            "siti-web": [
                "html",
                "css",
                "js",
                "yaml",
                "toml",
                "php",
                "xml",
                "scss"
            ],
            "database": [
                "sqlite",
                "db"
            ],
            "eseguibili": [
                "exe",
                "bin",
                "sh",
                "jar",
                "py",
                "pyc",
                "ipynb",
                "msi",
                "bat",
                "java",
                "cpp",
                "out",
                "ts"
            ],
        }
        
        # for any tuple of (filetype, [extension])
        for value in filetype.items():
            
            # if the extension is in the extension list of the tuple
            if self.extension in value[1]:
                
                # then return the filetype
                return value[0]

    
    # move the file
    def move(self, verbose):
        
        # source path of the file
        src = f"{self.path}/{self.filename}"
        
        # destination path of the file
        dst = f"{self.path}/{self.filetype}/{self.extension}/"
        
        # if the path for the filetype doesn't exist, make it
        if not os.path.exists(f"{self.path}/{self.filetype}"):
            os.mkdir(f"{self.path}/{self.filetype}")
            
        # if the path for the extension doesn't exist, make it
        if not os.path.exists(f"{self.path}/{self.filetype}/{self.extension}"):
            os.mkdir(f"{self.path}/{self.filetype}/{self.extension}")
        
        try:
            
            # try moving the file from src to dst
            shutil.move(src, dst)
        
            # if the verbose mode is print the line
            if verbose:
                print(f"+- [✔] -+ {self.filename} moved to {self.filetype}/{self.extension}.")
        
        # exception handling
        except shutil.Error as e:
            
            print(f"+- [!] -+ Error moving {self.filename} to {self.filetype}/{self.extension}, file already exists.")
