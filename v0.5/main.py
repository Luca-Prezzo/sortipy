#!/usr/bin/env python3

import class_file
import argparse


# main function, arguments are: verbose, hidden, copy
def main(path, verbose=False, hidden=False, copy=False):
    
    if copy:
        class_file.mkcopy(path)

            
    file_list = class_file.list_file(path, hidden)

    for file in file_list:
        current_file = class_file.File(path, file)
        current_file.move(verbose)


if __name__ == "__main__":
    
    # +-------------------+ ARGPARSE +-------------------+
    parser = argparse.ArgumentParser(description="file organizer written in python.")
    parser.add_argument("folder", type=str, help="Folder to sort.")
    parser.add_argument("-V", "--verbose", help="Enable verbose mode.", action="store_true")
    parser.add_argument("-H", "--hidden", help="Add hidden file to the sorting.", action="store_false")
    parser.add_argument("-C", "--copy", help="Make a copy named as folder_BACKUP before sorting.", action="store_true")
    args = parser.parse_args()
    # +--------------------------------------------------+
        
    main(path=args.folder, verbose=args.verbose, hidden=args.hidden, copy=args.copy)
