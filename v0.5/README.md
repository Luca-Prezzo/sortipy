# Python Organizer

- Autore: Luca Prezzo
- Descrizione: Organizza i file all'interno di una cartella.
- Versione: 0.4
---





[TOC]





## V0.1 - Funzioni di base

Nel momento in cui lo script viene lanciato ogni file della cartella in cui ci troviamo viene automaticamente spostato in una sottocartella in base alla sua estensione.

```
bash> ordina
```





## V0.2 - Gestione della directory e macro-categorie

Rispetto alla precedente versione i file vengono spostati in delle sottocartelle e divisi per categoria.

 Es:
- `testo.txt` verrà spostato nella cartella `testi` → `txt`;
- `video.mkv` verrà spostato nella cartella `video` → `mkv`.

Ora quando si invoca lo script da terminale bisogna specificare la cartella di destinazione, es. `ordina <cartella>`.

Il riconoscimento del tipo di file è ottenuto tramite un dizionario strutturato in `tipo: estensioni possibili` in modo da ciclare tra tutti valori e restituire la chiave associata.

```
bash> ordina "cartella da ordinare"
```





## V0.3 - Gestione degli argomenti e delle eccezioni

Ora la cartella da ordinare viene passata come argomento della librearia argparse.

Argomenti opzionali aggiunti:

- `-h` o `--help`, argomento generato automaticamente dalla libreria in questione;
- `-v` o `--verbose`, riproduce in modalità verbose l'output, ogni volta che un file viene spostato verrà mostrato il nome del file e la sua destinazione;
- `-e` o `--eccezioni`, permette di aggiungere nel passaggio successivo una serie di eccezioni, è importante scrivere il nome del file in modo corretto rispettando maiuscole e minuscole per essere sicure che il file venga riconosciuto.

Aggiunta inoltre la sezione per  i file dei progetti di Adobe e qualche estensione secondaria.

```
bash> ordina "cartella da ordinare" -v -e
```





## V0.4 - Copia di backup e errori

Aggiunto l'argomento opzionale `-c` o `--copia` che crea una copia di backup della cartella prima di ordinare la stessa.
La nuova cartella si chiama `<nomecartella>__originale`.

Aggiunto il messaggio di errore in caso di estensione non riconosciuta.

```
bash> ordina "cartella da ordinare" -c
```

Aggiunto l'argomento opzionale `-H` o `--hidden` per includere i file nascosti (file che iniziano con ".").



## V0.5 - Refactoring

Riscritto l'intero programma utilizzando la programmazione ad oggetti.
