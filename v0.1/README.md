# Python Organizer

- Autore: Luca Prezzo
- Descrizione: Organizza i file all'interno di una cartella.
- Versione: 0.1

---





[TOC]





## V0.1 - Funzioni di base

Nel momento in cui lo script viene lanciato ogni file della cartella in cui ci troviamo viene automaticamente spostato in una sottocartella in base alla sua estensione.

```
bash> ordina
```



