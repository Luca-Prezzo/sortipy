#!/usr/bin/env python3
from libmover import *
import shutil


def main(cartella: str):
    files = lista_file(cartella)

    if len(files) == 0:
        print("Non ci sono elementi da ordinare")

    else:
        for file in files:
            nome, estensione = get_filename(file)

            if os.path.exists(cartella + "/" + estensione):
                shutil.move(cartella + "/" + file, cartella + "/" + estensione + "/" + file)
            else:
                os.mkdir(cartella + "/" + estensione)
                shutil.move(cartella + "/" + file, cartella + "/" + estensione + "/" + file)


if __name__ == "__main__":
    cartella = os.getcwd()
    main(cartella)
