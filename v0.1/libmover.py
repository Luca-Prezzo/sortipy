import os


def lista_file(directory):
    return [elemento for elemento in os.listdir(directory) if os.path.isfile(f"{directory}/{elemento}")]


def get_filename(file):
    nome, ext = os.path.splitext(file)
    return nome, ext.lower().replace(".", "")


def crea_test():
    tipologie = {
        "testi": ["txt", "pdf", "docx", "doc", "tex", "md", "odt"],
        "foto": ["png", "jpeg", "jpg", "bpm", "png", "svg", "tif", "tiff"],
        "video": ["mp4", "3gp", "mkv", "avi", "gif"],
        "script": ["py", "pyc"]
    }
    for i in tipologie:
        for j in tipologie[i]:
            with open(f"/home/luca/test/prova.{j}", "w"):
                pass
