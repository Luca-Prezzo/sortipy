#!/usr/bin/env python3
from libmover import *
import argparse
import shutil


def main(cartella: str, verbose=False, eccezioni=None):
    files = lista_file(cartella)

    if len(files) == 0:
        print("Non ci sono elementi da ordinare")

    else:
        for file in files:
            if file not in eccezioni:
                nome, tipo, estensione = get_filename(file)

                if not os.path.exists(cartella + "/" + tipo):
                    os.mkdir(cartella + "/" + tipo)

                if not os.path.exists(cartella + "/" + tipo + "/" + estensione):
                    os.mkdir(cartella + "/" + tipo + "/" + estensione)

                try:
                    shutil.move(cartella + "/" + file, cartella + "/" + tipo + "/" + estensione + "/" + file)
                    if verbose:
                        print(f"+-- {file} spostato in {tipo} - {estensione}")
                except FileExistsError:
                    print("Il file esiste già.")


if __name__ == "__main__":

    # +-- inizio arg
    parser = argparse.ArgumentParser(description="file organizer written in python.")
    parser.add_argument("cartella", type=str, help="cartella da organizzare")
    parser.add_argument("-v", "--verbose", help="imposta la modalità verbose", action="store_true")
    parser.add_argument("-e", "--eccezioni", help="imposta le eccezioni", action="store_true")
    args = parser.parse_args()
    # +-- fine arg

    if args.eccezioni:
        ecc = gestisci_eccezioni()
    else:
        ecc = None

    main(args.cartella, args.verbose, ecc)
