import os


def lista_file(directory):
    return [elemento for elemento in os.listdir(directory) if os.path.isfile(f"{directory}/{elemento}")]


def get_filename(file):
    nome, ext = os.path.splitext(file)
    tipo = get_tipo(ext.lower().replace(".", ""))
    return nome, tipo, ext.lower().replace(".", "")


def gestisci_eccezioni():
    print("Inserisci per ogni riga quale file vuoi escludere.\nPremi invio quando hai finito.\n")
    ecc = []
    while 1:
        temp = input(">> ")
        if temp == "":
            break
        else:
            ecc.append(temp)
    return ecc


def get_tipo(estensione):
    filetype = {
        "testi": [
            "txt",
            "pdf",
            "docx",
            "doc",
            "tex",
            "md",
            "odt"
        ],
        "adobe": [
            "ai",
            "psd",
            "propj",
            "svg",
            "eps"
        ],
        "foto": [
            "png",
            "jpeg",
            "jpg",
            "bpm",
            "png",
            "svg",
            "tif",
            "tiff"
        ],
        "video": [
            "mp4",
            "3gp",
            "mkv",
            "avi",
            "gif",
            "mpeg",
            "mpg"
        ],
        "audio": [
            "aif",
            "cda",
            "mid",
            "mp3",
            "mpa",
            "ogg",
            "wav",
            "wma",
            "wpl"
        ],
        "archivio": [
            "7z",
            "deb",
            "pkg",
            "rpm",
            "gz",
            "z",
            "zip",
            "tar"
        ],
        "eseguibili": [
            "exe",
            "bin",
            "sh",
            "jar",
            "py",
            "msi",
            "bat"
        ],
    }
    # ciclo la tupla (chiave, valore) finchè non trovo l'estensione tra
    # i valori e restituisco la chiave associata
    for valori in filetype.items():
        if estensione in valori[1]:
            return valori[0]
