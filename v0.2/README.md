# Python Organizer

- Autore: Luca Prezzo
- Descrizione: Organizza i file all'interno di una cartella.
- Versione: 0.2

---





[TOC]





## V0.1 - Funzioni di base

Nel momento in cui lo script viene lanciato ogni file della cartella in cui ci troviamo viene automaticamente spostato in una sottocartella in base alla sua estensione.

```
bash> ordina
```





## V0.2 - Gestione della directory e macro-categorie

Rispetto alla precedente versione i file vengono spostati in delle sottocartelle e divisi per categoria.

 Es:

- `testo.txt` verrà spostato nella cartella `testi` → `txt`;
- `video.mkv` verrà spostato nella cartella `video` → `mkv`.

Ora quando si invoca lo script da terminale bisogna specificare la cartella di destinazione, es. `ordina <cartella>`.

Il riconoscimento del tipo di file è ottenuto tramite un dizionario strutturato in `tipo: estensioni possibili` in modo da ciclare tra tutti valori e restituire la chiave associata.

```
bash> ordina "cartella da ordinare"
```

