import os


def lista_file(directory):
    return [elemento for elemento in os.listdir(directory) if os.path.isfile(f"{directory}/{elemento}")]


def get_filename(file):
    nome, ext = os.path.splitext(file)
    tipo = get_tipo(ext.lower().replace(".", ""))
    return nome, tipo, ext.lower().replace(".", "")


def get_tipo(estensione):
    filetype = {
        "testi": [
            "txt",
            "pdf",
            "docx",
            "doc",
            "tex",
            "md",
            "odt"
        ],
        "foto": [
            "png",
            "jpeg",
            "jpg",
            "bpm",
            "png",
            "svg",
            "tif",
            "tiff"
        ],
        "video": [
            "mp4",
            "3gp",
            "mkv",
            "avi",
            "gif"
        ],
        "script": [
            "py",
            "pyc"
        ]
    }
    # ciclo la tupla (chiave, valore) finchè non trovo l'estensione tra
    # i valori e restituisco la chiave associata
    for valori in filetype.items():
        if estensione in valori[1]:
            return valori[0]

