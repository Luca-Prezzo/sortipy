#!/usr/bin/env python3
from libmover import *
import shutil
import sys


def main(cartella: str):
    files = lista_file(cartella)

    if len(files) == 0:
        print("Non ci sono elementi da ordinare")

    else:
        for file in files:

            nome, tipo, estensione = get_filename(file)

            if not os.path.exists(cartella+"/"+tipo):
                os.mkdir(cartella+"/"+tipo)

            if not os.path.exists(cartella+"/"+tipo+"/"+estensione):
                os.mkdir(cartella+"/"+tipo+"/"+estensione)

            try:
                shutil.move(cartella+"/"+file, cartella+"/"+tipo+"/"+estensione+"/"+file)
            except FileExistsError:
                print("Il file esiste già.")


if __name__ == "__main__":
    cartella = sys.argv[1]
    main(cartella)
